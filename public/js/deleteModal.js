
window.onload = function() {

    // ecouteur d'evenement 
    document.querySelectorAll('.confirm-delete').forEach(button => {
        button.addEventListener('click', function() {
            let id = this.dataset.id;
            

            let modal = document.querySelector('#exampleModal');

            let button = modal.querySelector('.btn-danger');
            button.href = this.dataset.href;

            var myModal = new bootstrap.Modal(modal);
            myModal.show();
        });
    });
}