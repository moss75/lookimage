<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\Query\Expr\Math;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Mmo\Faker\PicsumProvider;
use Symfony\Component\HttpFoundation\File\File;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{


    public function getDependencies()
    {
        return [
            CategorieFixtures::class,
            UserFixtures::class
        ];    
    }


    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create();
        // On ajout le bundle "Mmmo Faker-image" a faker
        $faker->addProvider(new PicsumProvider($faker));
        // Creation de la boucle pour boucler le nombre de fake donner qu'on desire 
        for($i=0; $i <=100; $i++ ) {

            $image = $faker->picsum('./public/uploads/images/photos', random_int(1152, 2312), random_int(865,1736));

            $category = $this->getReference('category_'. random_int(0,10));

            // Recupere une ref utilisateur aléatoirement

            $user = $this->getReference('user_' . random_int(0,10));
          
            
            $picture = new Picture;
            $picture->setDescription($faker->sentence(20));
            $picture->setTags($faker->word);
            $picture->setCreatedAt($faker->dateTimeBetween('-4 years'));
            $picture->setUpdatedAt($faker->dateTimeBetween('-2 years'));
            $picture->setUser($user);

           
            $picture->setCategory($category);

            // Gestion de l'image 
            $picture->setImageFile(new File($image));
            $picture->setImage(str_replace('./public/uploads/images/photos\\', '', $image));

            $manager->persist($picture);

        }


        $manager->flush();
    }
}
