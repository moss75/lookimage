<?php

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\Picture;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UploadPictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Selectionner une photo',
                // Option de vichImageType
                'allow_delete' => false,
                'download_uri' => false,
                'imagine_pattern' => 'thumb',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez choisir une photo',
                        'groups' => ['new-picture']
                    ]),
                    new Image([     
                        'maxSize' => '2G',
                        'maxSizeMessage' => 'Le fichier ne doit pas depasser les 2Mo',
                        'mimeTypes' => [
                            'image/gif', 
                            'image/png',
                            'image/jpeg',
                            'image/webp'
                        ],
                        'mimeTypesMessage' => 'Cette image est invalide, les formats acceptés sont : .png  .gif  .webp  .jepg'
                    ])
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => true, 
                'label' => 'Description de la photo', 
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir une description',
                        'groups' => ['new-picture', 'edit_picture']
                    ]),
                ]
            ])
            ->add('tags', TextType::class, [
                'required' => true,
                'label' => 'Tags',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un ou plusieur Tags',
                        'groups' => ['new-picture', 'edit_picture']
                    ])
                ]
                ])
            ->add('category', EntityType::class, [
                'required' => true,
                'label' => 'Categorie de la photo',
                'class' => Categorie::class,
                'choice_label' => 'name'
            ])
            ->add('is_Validated', ChoiceType::class, [
                'required' => true,
                'label' => 'afficher la photo ?',
                'choices' => [
                    'oui' => true,
                    'non' => false
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Picture::class,
        ]);
    }
}
