<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Picture;
use App\Form\CategoryType;
use App\Form\UploadPictureType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {

        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        return $this->render('admin/index.html.twig', [
            'categories' =>  $categories
        ]);
    }
    /**
     * @Route("/admin/categories/new", name="admin_categories_new")
     */
    public function new(Request $request)
    {
        // Instanciation de l'entité désirée
        $category = new Categorie();

        // Création du formulaire
        $formNewCategory = $this->createForm(CategoryType::class, $category);
        $formNewCategory->handleRequest($request);

        // Traitement des données si le formulaire est envoyé 
        // et que les validations sont acceptées
        if ($formNewCategory->isSubmitted() && $formNewCategory->isValid()) {
            // Tranforme le nom de la catégorie en slug
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            // Message flash
            $this->addFlash('success', 'La catégorie à bien été créée');

            // Retour à mon tableau de catégorie
            return $this->redirectToRoute('admin');
        }

        // Envoi de la vue
        return $this->render('admin/new.html.twig', [
            'formNewCategory' => $formNewCategory->createView()
        ]);
    }

   /**
     * @Route("/admin/categories/edit/{id}", name="admin_categories_edit")
     */
    public function edit($id, Request $request)
    {
        // Sélectionne la catégorie en BDD selon son ID
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        // Erreur 404 si la catégorie n'existe pas
        if (!$category) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas');
        }

        // Création du formulaire
        $formEditCategory = $this->createForm(CategoryType::class, $category);
        $formEditCategory->handleRequest($request);

        // Vérifie que le formulaire soit envoyé et validé
        if ($formEditCategory->isSubmitted() && $formEditCategory->isValid()) {
            // Tranforme le nom de la catégorie en slug
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // Mise à jour en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            // Message flash
            $this->addFlash('success', 'La catégorie à bien été modifiée');

            // Retour sur le listing des catégories
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/edit.html.twig', [
            'formEditCategory' => $formEditCategory->createView()
        ]);
    }
     /**
     * @Route("/admin/delete/categories/{id}", name="admin_delete_categories")
     */
    public function delete($id)
    {
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        if(!$category) {
            throw $this->createNotFoundException('cette image n\'existe pas');
        }
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($category);
        $doctrine->flush();

        $this->addFlash('success', 'la categorie a bien été supprier');
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/PictureAdmin", name="Picture_admin")
     */
    public function pictureAdmin(Request $request, PaginatorInterface $paginator)
    {

        
        $page = $request->query->getInt('page', 1);

        $query = $this->getDoctrine()->getRepository(Picture::class)->findAll();

        $pictures = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page, 
            15
        );

        return $this->render('admin/pictureAdmin.html.twig', [
            'pictures' =>  $pictures
        ]);
    }
    /**
     * @Route("/admin/categories/editPicture/{id}", name="admin_Picture_edit")
     */
    public function editPicture($id, Request $request)
    {
        // Sélectionne la catégorie en BDD selon son ID
        $pictures = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Erreur 404 si la catégorie n'existe pas
        if (!$pictures) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas');
        }

        // Création du formulaire
        $formEditPictures = $this->createForm(UploadPictureType::class, $pictures);
        $formEditPictures->handleRequest($request);

        // Vérifie que le formulaire soit envoyé et validé
        if ($formEditPictures->isSubmitted() && $formEditPictures->isValid()) {
            // Tranforme le nom de la catégorie en slug
            // $pictures->setSlug((new AsciiSlugger())->slug(strtolower($pictures->getName())));

            // Mise à jour en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($pictures);
            $doctrine->flush();

            // Message flash
            $this->addFlash('success', 'La catégorie à bien été modifiée');

            // Retour sur le listing des catégories
            return $this->redirectToRoute('Picture_admin');
        }

        return $this->render('admin/editPicture.html.twig', [
            'formEditPictures' => $formEditPictures->createView()
        ]);
    }
    /**
     * @Route("/admin/delete/picture/{id}", name="admin_delete_picture")
     */
    public function PictureDelete($id)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
        // if(!$picture || $this->getUser() !== $picture->getUser() ) {
        //     throw $this->createNotFoundException('cette image n\'existe pas');
        // }
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success', 'la photo a bien été supprier');
        return $this->redirectToRoute('Picture_admin');
    }
}
