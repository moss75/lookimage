<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Form\UploadPictureType;
use Doctrine\ORM\Mapping\Id;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {

        $page = $request->query->getInt('page', 1);

        $query = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'user' => $this->getUser()
        ]);
        $pictures = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page, 
            3
        );

        return $this->render('account/index.html.twig', [
            'pictures' => $pictures

        ]);
    }

    /**
     * @Route("/account/new/picture", name="account_new_picture")
     */
    public function new(Request $request)
    {
        // Instancie l'entité "Picture"
        $picture = new Picture();

        // Premier paramètre : Le formulaire dont ont a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formUpload = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'new_picture'
        ]);
        $formUpload->handleRequest($request);
        
        // Vérifie si le formulaire est envoyé et valide !
        if ($formUpload->isSubmitted() && $formUpload->isValid()) {
            
            // Ajoute la date du jour
            $picture->setCreatedAt(new \DateTimeImmutable());

            // Passe l'utilisateur actuellement connecté à notre setter
            $picture->setUser($this->getUser());

            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Merci pour votre partage !');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('account');
        }

        // Si vous voulez capter toutes les erreurs d'un formulaire pour les afficher
        // où vous le souhaitez.
        /* if ($formUpload->isSubmitted() && !$formUpload->isValid()) {
            $errors = $formUpload->getErrors(true);
        } */

        return $this->render('account/new.html.twig', [
            'formUpload' => $formUpload->createView(),
            // 'errors' => $errors
        ]);
    }
     /**
     * @Route("/account/edit/picture/{id}", name="account_edit_picture")
     */
    public function edit($id, Request $request)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        if(!$picture || $this->getUser() !== $picture->getUser() ) {
            throw $this->createNotFoundException('cette image n\'existe pas');
        }
        $formEdit = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'edit_picture'
        ]);
        $formEdit->handleRequest($request);

        if ($formEdit->isSubmitted() && $formEdit->isValid())
        {
            $picture->setUpdatedAt(new \DateTimeImmutable());

            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            $this->addFlash('succes', 'les modification on bien été enregistrées');

            return $this->redirectToRoute('account');
        }
        
        return $this->render('account/edit.html.twig', [
            'picture' => $picture,
            'formUpload' => $formEdit->createView()
        ]);
    }
     /**
     * @Route("/account/delete/picture/{id}", name="account_delete_picture")
     */
    public function delete($id)
    {
        $picture = $this->getDoctrine()->getRepository(UploadPictureType::class)->find($id);
        if(!$picture || $this->getUser() !== $picture->getUser() ) {
            throw $this->createNotFoundException('cette image n\'existe pas');
        }
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success', 'la photo a bien été supprier');
        return $this->redirectToRoute('account');
    }
}


