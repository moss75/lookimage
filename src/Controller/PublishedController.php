<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublishedController extends AbstractController
{
    /**
     * @Route("/published", name="published")
     */
    public function index(): Response
    {
        return $this->render('published/index.html.twig', [
            'controller_name' => 'PublishedController',
        ]);
    }
}
