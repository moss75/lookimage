<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\String\Slugger\AsciiSlugger;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        // Instancier Faker Pour pouvoir l'utiliser

        $faker = Faker\Factory::create();

        // Cree une boucle for pour choisir le nom d'element alland en bdd

        for($i = 0; $i <= 10; $i++) {
            $category = new Categorie();
            $category->setName($faker->colorName);
            $category->setSlug((new AsciiSlugger())->slug(strtolower($faker->colorName)));
            
            $this->addReference('category_'.$i, $category);
            // garde de coter en attendant l'execution des requette
            $manager->persist($category);
        }

        $manager->flush();
    }
}
