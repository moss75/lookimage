<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditUserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render('user/index.html.twig', [
            'users' => $users
        ]);
    }
     /**
     * @Route("/user/edit/{id}", name="user_edit")
     */
    public function editUser($id, Request $request){
                // Sélectionne la catégorie en BDD selon son ID
                $users = $this->getDoctrine()->getRepository(User::class)->find($id);

                
                // Erreur 404 si la catégorie n'existe pas
                if (!$users) {
                    throw $this->createNotFoundException('Cette utilisateur n\'existe pas');
                }
                $formEditUser = $this->createForm(EditUserType::class, $users);
                $formEditUser->handleRequest($request);

                if ($formEditUser->isSubmitted() && $formEditUser->isValid()) {
                    $doctrine = $this->getDoctrine()->getManager();
                    $doctrine->persist($users);
                    $doctrine->flush();
            
                    $this->addFlash('message', 'Utilisateur modifié avec succès');
                    return $this->redirectToRoute('user');
                }



        return $this->render('user/editUser.html.twig', [
            'formEditUser' => $formEditUser->createView()
        ]);
    }
    /**
     * @Route("/user/delete/{id}", name="user_delete")
     */
    public function deleteUser($id)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->find($id);
        // if(!$picture || $this->getUser() !== $picture->getUser() ) {
        //     throw $this->createNotFoundException('cette image n\'existe pas');
        // }
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($users);
        $doctrine->flush();

        $this->addFlash('success', 'l\'utilisateur a bien ete supprimer');
        return $this->redirectToRoute('user');
    }

}


