<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Picture;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $donnees = $this->getDoctrine()->getRepository(Picture::class)->findby(['is_validated' => true]);

        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        
        $pictures = $paginator->paginate($donnees, $request->query->getInt('page',1), 5);
      

        return $this->render('home/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories
        ]);
        
    }

      /**
     * @Route("/category/{id}", name="pictures_by_category")
     */
    public function pictureByCategory($id, Request $request, PaginatorInterface $paginator)
    {
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        $donnees = $this->getDoctrine()->getRepository(Picture::class)->findby([
            'category' => $category,
            'is_validated' => true]);

        if(!$category) {
            throw $this->createNotFoundException('la categorie n\'exizte pas');
        }
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $donnees,
            $page === 0 ? 1: $page,
            5
        );
        return $this->render('home/categorie.html.twig', [
            'category' => $category,
            'pictures' => $pictures
        ]);
    }
    
    /**
     * @Route("/Picture/{id}", name="picture_by_id")
     */
    public function pictureById($id, Request $request, PaginatorInterface $paginator)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        if(!$picture) {
            throw $this->createNotFoundException('l\'image n\'exizte pas');
        }
       
        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
            $picture->getCategory()->getPictures(),
            $page === 0 ? 1 : $page,
            5
        );
        return $this->render('home/picture.html.twig', [
            'picture' => $picture,
            'photos' => $photos

        ]);
    }


    
}

